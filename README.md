Introduction to Microcontrollers
--------------------------------

Course materials for the Intro to Microcontrollers course.

 * [Course text](intro-to-micro.md)
 * [Example sketches](examples/)
 * [Arduino UNO pinout](uno_pinout.pdf)
